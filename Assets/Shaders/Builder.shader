﻿Shader "Custom/Builder" {
    Properties {
        _Color("Color", Color) = (1, 1, 1, 1)
        _MainTex("Top", 2D) = "white" {}
        _SideTex("Side", 2D) = "white" {}
        _DispTex("Displacement", 2D) = "white" {}
        _Glossiness("Smoothness", Range(0,1)) = 0.5
        _Metallic("Metallic", Range(0,1)) = 0.0
    }

    SubShader {
        Tags { "RenderType" = "Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard vertex:vert tessellate:tess addshadow fullforwardshadows
        #pragma target 5.0
        #include "Tessellation.cginc"

        struct appdata {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
            float4 texcoord : TEXCOORD0;
            float4 color : COLOR;
        };

        struct Input {
            float2 uv_MainTex;
            float4 normal : COLOR;
        };

        fixed4 _Color;
        sampler2D _MainTex;
        sampler2D _SideTex;
        sampler2D _DispTex;
        float _Glossiness;
        float _Metallic;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        float4 tess(appdata i0, appdata i1, appdata i2) {
            return UnityDistanceBasedTess(i0.vertex, i1.vertex, i2.vertex, 5, 50, 100);
        }

        void vert(inout appdata i) {
            float d = tex2Dlod(_DispTex, float4(i.texcoord.xy, 0, 0)).r * 0.1;
            i.vertex.xyz += i.normal * d;
            i.color = float4(i.normal, 1);
        }

        void surf(Input i, inout SurfaceOutputStandard o) {
            if (i.normal.y > 0.7 || i.normal.y < -0.7) {
                o.Albedo = (tex2D(_MainTex, i.uv_MainTex) * _Color).rgb;
            }
            else {
                o.Albedo = (tex2D(_SideTex, i.uv_MainTex) * _Color).rgb;
            }
            //o.Albedo = i.normal.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
        }
        ENDCG
    }
    FallBack "Standard"
}

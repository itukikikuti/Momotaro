Shader "Custom/Sky"
{
    Properties
    {
    	_rump("Rump Texture", 2D) = "white"{}
        _sunIntensity("SunIntensity", Float) = 1
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Background"
            "RenderType" = "Background"
            "PreviewType" = "Skybox"
        }
        Cull Off
        ZWrite off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct VertexInput
            {
                float4 vertex : POSITION;
            };

            struct VertexOutput
            {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
            };

            sampler2D _rump;
            float _sunIntensity;
            
            VertexOutput vert (VertexInput v)
            {
                VertexOutput o = (VertexOutput)0;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }

            float4 frag(VertexOutput i) : SV_Target
            {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;

                float u = (lightDirection.z + 1) / 4;
                float v = -viewDirection.y;

                if (lightDirection.y < 0)
                {
                	u = -(u - 0.5) + 0.5;
                }

                float sunStep = step(0.999, dot(-lightDirection, viewDirection));
                float3 sun = sunStep + lightColor * sunStep * _sunIntensity;
                float3 sky = tex2D(_rump, float2(u, v)) * (1 - sunStep);
                return float4(sun + sky, 1);
            }
            ENDCG
        }
    }
}

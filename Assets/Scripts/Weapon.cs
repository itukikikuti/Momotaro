﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : Item
{
    [SerializeField] protected float speed;
    [SerializeField] protected float knockback;

    [HideInInspector] public Character owner;

    public abstract IEnumerator Attack();
    public abstract IEnumerator Sheathe();
}

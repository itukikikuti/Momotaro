﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Katana : Weapon
{
    bool isAttack = false;

    public override IEnumerator Attack()
    {
        isAttack = true;

        yield return new WaitUntil(() => Input.GetAxis("Fire1") > 0.001f);

        if (Input.GetAxis("Fire1") > 0.001f)
        {
            yield return Slash(new Vector3(-1f, -0.3f, 0f));
        }
        if (Input.GetAxis("Fire1") > 0.001f)
        {
            yield return Slash(new Vector3(1f, -0.3f, 0f));
        }
        if (Input.GetAxis("Fire1") > 0.001f)
        {
            yield return Slash(new Vector3(-1f, 0f, 0f));
        }
        if (Input.GetAxis("Fire1") > 0.001f)
        {
            yield return Slash(new Vector3(1f, -0.4f, 0f));
        }
        if (Input.GetAxis("Fire1") > 0.001f)
        {
            yield return new WaitForSeconds(speed);
            yield return Slash(new Vector3(-1f, 0f, 0f));
        }

        isAttack = false;
    }

    public override IEnumerator Sheathe()
    {
        if (Input.GetAxis("Fire1") > 0.001f)
        {
            yield break;
        }

        bool isComplete = false;

        Sequence sequence = DOTween.Sequence()
            .Join(transform.DOLocalMove(new Vector3(-0.3f, 0.8f, 0.3f), 0.1f))
            .Join(transform.DOLocalRotateQuaternion(Quaternion.Euler(-20f, 0f, 0f), 0.1f));

        sequence.onComplete = () => isComplete = true;
        yield return new WaitUntil(() => isComplete);
    }

    public IEnumerator Slash(Vector3 start)
    {
        bool isComplete = false;
        start.y += owner.shoulder.localPosition.y;
        Vector3 center = new Vector3(0f, owner.shoulder.localPosition.y, 1f);
        Vector3 offset = center - start;
        Vector3 end = center + offset;
        end.z = start.z;

        Tweener tweener = transform.DOLocalPath(new Vector3[]{ start, center, end }, speed, PathType.CatmullRom).SetLookAt(owner.shoulder).SetEase(Ease.InOutCubic);

        tweener.onComplete = () => isComplete = true;
        yield return new WaitUntil(() => isComplete);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (!isAttack)
        {
            return;
        }

        if (collider.attachedRigidbody == null)
        {
            return;
        }

        collider.attachedRigidbody.AddExplosionForce(knockback, owner.shoulder.localPosition, Mathf.Infinity);
    }
}

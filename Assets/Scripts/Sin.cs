﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sin : MonoBehaviour
{
    [SerializeField] float radian;
    [SerializeField] float speed;
    [SerializeField] float scale;

    void Start()
    {

    }

    void Update()
    {
        radian += speed;
        if (radian > Mathf.PI * 2f)
        {
            radian = 0f;
        }

        transform.localPosition = new Vector3(radian, Mathf.Sin(radian), 0f) * scale;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snap : MonoBehaviour
{

    private void Awake()
    {
        Transform parent = this.transform.parent;
        this.transform.SetParent(null);

        HumanDescription humanDesc = new HumanDescription();
        humanDesc.human = GetHuman();
        humanDesc.skeleton = GetSkeleton();
        humanDesc.upperArmTwist = 0.5f;
        humanDesc.lowerArmTwist = 0.5f;
        humanDesc.upperLegTwist = 0.5f;
        humanDesc.lowerLegTwist = 0.5f;
        humanDesc.armStretch = 0.05f;
        humanDesc.legStretch = 0.05f;
        humanDesc.feetSpacing = 0.0f;
        humanDesc.hasTranslationDoF = false;

        Avatar avatar = AvatarBuilder.BuildHumanAvatar(this.gameObject, humanDesc);

        this.GetComponent<Animator>().avatar = avatar;
        this.transform.SetParent(parent);
    }

    HumanBone[] GetHuman()
    {
        List<HumanBone> human = new List<HumanBone>();
        GetChildren(this.transform, (bone) =>
        {
            HumanBone humanBone = new HumanBone();
            humanBone.humanName = bone.name;
            humanBone.boneName = bone.name;
            humanBone.limit.useDefaultValues = true;

            human.Add(humanBone);
        });

        return human.ToArray();
    }

    SkeletonBone[] GetSkeleton()
    {
        List<SkeletonBone> skeleton = new List<SkeletonBone>();
        GetChildren(this.transform, (bone) =>
        {
            SkeletonBone skeletonBone = new SkeletonBone();
            skeletonBone.name = bone.name;
            skeletonBone.position = bone.localPosition;
            skeletonBone.rotation = bone.localRotation;
            skeletonBone.scale = Vector3.one;

            skeleton.Add(skeletonBone);
        });

        return skeleton.ToArray();
    }

    void GetChildren(Transform root, Action<Transform> action)
    {
        foreach (Transform child in root)
        {
            action(child);
            GetChildren(child, action);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class PauseMenu : MonoBehaviour
{
    private bool isOpen = false;
    private CanvasGroup canvasGroup;

    private void Start()
    {
        canvasGroup = this.GetComponent<CanvasGroup>();

        Close();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (isOpen)
                Close();
            else
                Open();
        }

        if (isOpen)
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 1f, 0.1f);
        else
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 0f, 0.1f);
    }

    public void Open()
    {
        isOpen = true;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void Close()
    {
        isOpen = false;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        Time.timeScale = 1f;
        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;
    }
}

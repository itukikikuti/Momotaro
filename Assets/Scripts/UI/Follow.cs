﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Canvas))]
[RequireComponent(typeof(CanvasGroup))]
public class Follow : MonoBehaviour
{
    [SerializeField] public Transform target;

    private RectTransform rectTransform;
    private Canvas canvas;
    private CanvasGroup canvasGroup;

    private void Start()
    {
        rectTransform = this.GetComponent<RectTransform>();
        canvas = this.GetComponent<Canvas>();
        canvasGroup = this.GetComponent<CanvasGroup>();
    }

    private void Update()
    {
        Vector3 screen = Camera.main.WorldToScreenPoint(target.position);

        //Vector2 local;
        //RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.rootCanvas.GetComponent<RectTransform>(), screen, Camera.main, out local);

        if (screen.z > 0f)
            this.canvasGroup.alpha = 1f;
        else
            this.canvasGroup.alpha = 0f;

        //Vector2 size = canvas.rootCanvas.GetComponent<RectTransform>().sizeDelta / 2f - rectTransform.sizeDelta / 2f;
        //local = new Vector3(Mathf.Clamp(local.x, -size.x, size.x), Mathf.Clamp(local.y, -size.y, size.y), 0f);

        this.rectTransform.position = screen;
    }
}

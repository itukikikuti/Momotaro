﻿using UnityEngine;

public class CellTransform : Transform
{
    public Vector2Int cell
    {
        get;
        set;
    }
}

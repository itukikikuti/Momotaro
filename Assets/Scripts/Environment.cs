﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour
{
    [SerializeField] private Vector3 direction;
    [SerializeField] private float speed;
    [SerializeField] private Texture2D skyRumpTexture;
    [SerializeField] private Texture2D sunRumpTexture;
    [SerializeField] private Light light;
    [SerializeField] private LensFlare lensFlare;
    [SerializeField] float angle;

	void Update()
    {
        angle += Time.deltaTime * speed;
        transform.rotation = Quaternion.AngleAxis(angle, direction.normalized);

        Vector3 lightDirection = -transform.forward;
        float u = (lightDirection.z + 1f) / 4f;
        if (lightDirection.y < 0f)
        {
            u = -(u - 0.5f) + 0.5f;
        }

        light.color = sunRumpTexture.GetPixelBilinear(u - 1f / 16f, 0f);
        lensFlare.color = sunRumpTexture.GetPixelBilinear(u - 1f / 16f, 0f);

        RenderSettings.fogColor = skyRumpTexture.GetPixelBilinear(u - 1f / 32f, 0.5f);
	}
}

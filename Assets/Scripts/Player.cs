﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    [SerializeField] private float eyeHeight;
    [SerializeField] private float cameraMaxDistance;
    [SerializeField] private Vector2 cameraAngleRange;
    [SerializeField] private float moveSpeed;

    Rigidbody rigidbody;
    Vector3 cameraAngles;
    Quaternion cameraRotation;
    Animator animator;
    float speed;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        cameraAngles = transform.eulerAngles + new Vector3(0f, 180f, 0f);
        cameraRotation = Quaternion.Euler(cameraAngles);
        animator = this.GetComponentInChildren<Animator>();
    }

    void Update()
    {
        cameraAngles += new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0f) * Time.deltaTime;
        cameraAngles.x = Mathf.Clamp(cameraAngles.x, cameraAngleRange.x, cameraAngleRange.y);
        cameraRotation = Quaternion.Lerp(cameraRotation, Quaternion.Euler(cameraAngles), Time.deltaTime * 10f);
        Vector3 cameraDirection = cameraRotation * Vector3.forward;

        Vector3 cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1f, 0f, 1f)).normalized;
        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical")).normalized;
        if (Mathf.Abs(moveDirection.x) > 0f || Mathf.Abs(moveDirection.z) > 0f)
        {
            Quaternion targetRotation = Quaternion.LookRotation(cameraForward) * Quaternion.LookRotation(moveDirection);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 0.3f);

            speed += moveSpeed;
        }
        speed = Mathf.Min(Mathf.MoveTowards(speed, 0f, 1f), 10f);

        Vector3 velocity = Vector3.zero;

        float stepHeight = 0.5f;
        Ray ray = new Ray(this.transform.position + new Vector3(transform.forward.x, 1f, transform.forward.z) * stepHeight, Vector3.down);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.distance < stepHeight * 2f)
            {
                Debug.DrawLine(ray.origin, hit.point, Color.red);
                velocity = (hit.point - this.transform.position).normalized * speed;
            }
            else
            {
                Debug.DrawLine(ray.origin, hit.point, Color.green);
                velocity = rigidbody.velocity;
                //velocity = this.transform.forward * speed * 0.1f;
                //velocity.y = rigidbody.velocity.y;
            }
        }

        rigidbody.velocity = velocity;
        animator.SetFloat("speed", speed);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbody.velocity += new Vector3(0f, 50f, 0f);
        }

        float cameraDistance = cameraMaxDistance;
        Vector3 cameraOrigin = transform.position + new Vector3(0f, eyeHeight, 0f);
        if (Physics.SphereCast(cameraOrigin, Camera.main.nearClipPlane, cameraDirection, out hit, cameraMaxDistance, gameObject.layer))
        {
            cameraDistance = hit.distance;
        }
        if (Physics.CheckSphere(cameraOrigin, Camera.main.nearClipPlane, gameObject.layer))
        {
            cameraDistance = 0.1f;
        }
        Camera.main.transform.position = cameraOrigin + cameraDirection * cameraDistance;
        Camera.main.transform.LookAt(cameraOrigin);
    }
}
